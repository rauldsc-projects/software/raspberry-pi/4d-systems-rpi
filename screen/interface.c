#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <math.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <geniePi.h>  //the ViSi-Genie-RaspPi library

#define PORT 8080

char buffer[1024] = {0};
int sock = 0, ret;
struct genieReplyStruct reply;

int create_socket_and_connect(struct sockaddr_in *serv_addr){
  int sock, opt=1;
  // Create the socket
  // AF_INET = IPv4, AF_INET6 = IPv6
  sock = socket(AF_INET, SOCK_STREAM, 0);

  // Set wait timeout
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 300;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

  serv_addr->sin_family = AF_INET;
  serv_addr->sin_port = htons(PORT);
  // Convert IPv4 and IPv6 addresses from text to binary form
  inet_pton(AF_INET, "0.0.0.0", &(serv_addr->sin_addr));

  connect(sock, (struct sockaddr *)serv_addr, sizeof(*serv_addr));

  return sock;
}

void handleGenieEvent(struct genieReplyStruct * reply, int socket){
  if(reply->cmd == GENIE_REPORT_EVENT)    //check if the cmd byte is a report event
  {
    if(reply->object == GENIE_OBJ_USERBUTTON) //check if the object byte is that of a slider
      {
        genieWriteObj(GENIE_OBJ_FORM,1,0);
        if(reply->index == 0) send(socket, "coctel1", strlen("coctel1"), 0);
        if(reply->index == 1) send(socket, "coctel2", strlen("coctel2"), 0);
        if(reply->index == 2) send(socket, "coctel3", strlen("coctel3"), 0);
        if(reply->index == 3) send(socket, "coctel4", strlen("coctel4"), 0);
        if(reply->index == 4) send(socket, "coctel5", strlen("coctel5"), 0);
        if(reply->index == 5) send(socket, "coctel6", strlen("coctel6"), 0);
        if(reply->index == 6) send(socket, "coctel7", strlen("coctel7"), 0);	
        if(reply->index == 7) send(socket, "coctel8", strlen("coctel8"), 0);
        if(reply->index == 8) send(socket, "coctel9", strlen("coctel9"), 0);	
        if(reply->index == 9) send(socket, "coctel10", strlen("coctel10"), 0);
        if(reply->index == 10) send(socket, "coctel11", strlen("coctel11"), 0);
        if(reply->index == 11) send(socket, "coctel12", strlen("coctel12"), 0);
        while(recv(sock, buffer, 1024, MSG_WAITALL) != "completed"){
          delay(100);
        }				
      }
  }
}

void init(){
    send(sock, "init", strlen("init"), 0);
    for(int i=0; i < 12; i++){
      for(int j=0;j<1024;j++){
        buffer[j] = 0;
      }
      ret = recv(sock, buffer, 1024, MSG_WAITALL);
      genieWriteStr(0x00 + i , buffer);
      genieWriteObj(GENIE_OBJ_USERBUTTON,i,i+1);
      delay(1000);
    }
}

void setup(){
    //Socket stuffs
    struct sockaddr_in serv_addr;
    sock = create_socket_and_connect(&serv_addr);

    //Screen stuffs
    //pthread_t myThread;
    genieSetup("/dev/ttyUSB0", 9600);
    genieWriteObj(GENIE_OBJ_FORM,1,0);
    for(int j=0;j<1024;j++){
        buffer[j] = 0;
    }
    ret = recv(sock, buffer, 1024, MSG_WAITALL);
    while( buffer != "completed"){
      init();
    }
    genieWriteObj(GENIE_OBJ_FORM,0,0);
}

void loop(){
  while(1){
    while(genieReplyAvail()){
      genieGetReply(&reply);      //take out a message from the events buffer
      handleGenieEvent(&reply, sock);   //call the event handler to process the message
    }

    ret = recv(sock, buffer, 1024, MSG_WAITALL);
    delay(1000);
    }
}

int main(){
  setup();
  loop();
  
}

